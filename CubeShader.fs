#version 330 core

out vec4 FragColor;

in VS_OUT {
	in vec3 FragPos;
	in vec3 Normal;
	in vec2 TexCoords;
} fs_in;


uniform sampler2D tex;

uniform vec3 lightPos;
uniform vec3 viewPos;


void main() {

	vec3 color = texture(tex, fs_in.TexCoords).rgb;

	//ambient
	vec3 ambient = 0.55 * color;
	//diffuse
	vec3 lightDir = normalize(lightPos - fs_in.FragPos);
	vec3 normal = normalize(fs_in.Normal);
	float diff = max(dot(lightDir, normal), 0.0);

	vec3 diffuse = diff * color;

	//specular
	vec3 viewDir = normalize(viewPos - fs_in.FragPos);
	vec3 halfwayDir = normalize(viewDir + lightDir);

	float spec = pow(max(dot(halfwayDir, normal), 0.0), 32.0);

	vec3 specular = spec * color;
	

	FragColor = vec4(ambient + diffuse + specular, 1.0);
}