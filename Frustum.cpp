#include "Frustum.h"


#define ANG2RAD 3.14159265358979323846/180.0


Frustum::Frustum() {
}


Frustum::~Frustum() {
}



void Frustum::setCamInternals( float angle, float ratio, float near, float far) {

	this->ratio = ratio;
	this->angle = angle;
	this->nearD = near;
	this->farD = far;

	tang = ( float ) tan( ANG2RAD * this->angle * 0.5 );

	nh = this->nearD * tang;
	nw = this->nh * this->ratio;

	fh = this->farD * tang;
	fw = fh * this->ratio;

}

void Frustum::computeCamPlanes( glm::vec3 & p, glm::vec3 & l, glm::vec3 & u ) {

	glm::vec3 direction;
	glm::vec3 nearCenter;
	glm::vec3 farCenter;
	glm::vec3 X;
	glm::vec3 Y;
	glm::vec3 Z;

	//compute the z axis of the camera
	Z = p - l;
	Z = glm::normalize( Z );

	//x axis of camera with given "up" vector and z axis
	X = glm::cross( u, Z );
	X = glm::normalize( X );

	//the real up vector is the cross product of Z and X
	Y = glm::cross( Z, X );
	Y = glm::normalize( Y );

	//compute centers
	nearCenter = p - Z * nearD;
	farCenter = p - Z * farD;

	//compute 4 corners of the near plane
	ntl = nearCenter + Y * nh - X * nw;
	ntr = nearCenter + Y * nh + X * nw;
	nbl = nearCenter - Y * nh - X * nw;
	nbr = nearCenter - Y * nh + X * nw;

	//compute 4 cornders of the far plane
	ftl = farCenter + Y * fh - X * fw;
	ftr = farCenter + Y * fh + X * fw;
	fbl = farCenter - Y * fh - X * fw;
	fbr = farCenter - Y * fh + X * fw;

	//compute the six planes
	planes[TOP].createPlane( ntr, ntl, ftl );
	planes[BOTTOM].createPlane( nbl, nbr, fbr );
	planes[LEFT].createPlane( ntl, nbl, fbl );
	planes[RIGHT].createPlane( nbr, ntr, fbr );
	planes[NEARP].createPlane( ntl, ntr, nbr );
	planes[FARP].createPlane( ftr, ftl, fbl );

}


int Frustum::pointInFrustum( glm::vec3 & cubeVertices ) {

	int result = INSIDE;

	//test if any of the box corners is inside the camera frustum
	for ( int i = 0; i != 6; ++i ) {
		if ( planes[i].distance( cubeVertices ) < 0 ) {
			return OUTSIDE;
		}
	}

	return result;
}