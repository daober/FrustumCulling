#include <glad/glad.h>
#include <glfw3.h>
#include "stb_image.h"

#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>
#include <glm\glm\gtx\quaternion.hpp>
#include <glm/glm/gtc/type_ptr.hpp>
#include <glm\glm\gtx\matrix_decompose.hpp>


#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "Frustum.h"

#include <iostream>
#include <filesystem>

#include <ft2build.h>
#include FT_FREETYPE_H



void framebuffer_size_callback( GLFWwindow* window, int width, int height );
void mouse_callback( GLFWwindow* window, double xpos, double ypos );
void scroll_callback( GLFWwindow* window, double xoffset, double yoffset );
void processInput( GLFWwindow *window );

unsigned int loadTexture( char const * path );
int loadFont();


void renderPlane( Shader &shader, unsigned int tex );
void renderSphere( Shader &shader, Model &sphere, const glm::mat4& model );
void renderBoundingBoxWireframe( Shader &shader, const std::vector<glm::vec4>& sphereWorldPos );
void renderText( Shader &shader, std::string text, glm::fvec2 pos, float scale, glm::vec3 color );

std::vector<glm::vec3> BoundingBoxInWorldSpace( const std::vector<glm::vec3>& cubeVertices,  glm::mat4& model);
std::vector<glm::vec3> WorldSpaceAABB;

// settings
const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;

// camera
Camera camera( glm::vec3( 0.0f, 0.0f, 3.0f ) );
//lighting info
glm::vec3 lightPos( 3.0f, 3.0f, 0.0f );


float lastX = ( float ) SCR_WIDTH / 2.0;
float lastY = ( float ) SCR_HEIGHT / 2.0;
bool firstMouse = true;


// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;


struct Character {
	GLuint		TextureID;
	glm::ivec2	size;
	glm::ivec2	bearing;
	GLuint		Advance;
};

std::map<GLchar, Character> Characters;

FT_Library library;
FT_Face	   face;

unsigned int VAOTEXT;
unsigned int VBOTEXT;

unsigned int CUBEVAO;
unsigned int CUBEVBO;

unsigned int PLANEVAO;
unsigned int PLANEVBO;

unsigned int BOUNDINGVAO;
unsigned int BOUNDINGVBO;

unsigned int cubeCounter = 0;


Frustum frustum;

glm::mat4 projection;
glm::mat4 view;

int main() {


	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
	glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );



	// glfw window creation
	// --------------------
	GLFWwindow* window = glfwCreateWindow( SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL );
	if ( window == NULL ) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent( window );
	glfwSetFramebufferSizeCallback( window, framebuffer_size_callback );
	glfwSetCursorPosCallback( window, mouse_callback );
	glfwSetScrollCallback( window, scroll_callback );

	// tell GLFW to capture our mouse
	glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if ( !gladLoadGLLoader( ( GLADloadproc ) glfwGetProcAddress ) ) {
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}


	glEnable( GL_DEPTH_TEST );

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );


	Shader PlaneShader( "PlaneShader.vs", "PlaneShader.fs" );
	Shader CubeShader( "CubeShader.vs", "CubeShader.fs" );
	Shader TextShader( "TextShader.vs", "TextShader.fs" );
	Shader SphereShader( "SphereShader.vs", "SphereShader.fs" );
	Shader BoundingBoxShader( "BoundingShader.vs", "BoundingShader.fs" );

	loadFont();

	//needs to be called after loading the glad opengl function pointers since model contains opengl calls
	Model sphereModel( "Sphere/Sphere.obj" );

	//load textures
	unsigned int floorTexture = loadTexture( "wood.png" );


	PlaneShader.use();
	PlaneShader.setInt( "tex", 0 );


	// render loop
	// -----------
	while ( !glfwWindowShouldClose( window ) ) {
		
		float currentFrame = glfwGetTime();

		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		glm::mat4 projection = glm::perspective( glm::radians( camera.Zoom ), ( float ) SCR_WIDTH / ( float ) SCR_HEIGHT, 0.1f, 100.0f );
		glm::mat4 view = camera.GetViewMatrix();


		//frustum calculation
		frustum.setCamInternals( camera.Zoom, ( float ) SCR_WIDTH / ( float ) SCR_HEIGHT, 0.1f, 100.0f );
		frustum.computeCamPlanes( camera.Position, camera.Position + camera.Direction, camera.Up );


		// input
		// -----
		processInput( window );

		// render
		// ------
		glClearColor( 0.1f, 0.1f, 0.1f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		renderPlane(PlaneShader, floorTexture);

		renderText( TextShader, "Frustum Culling Demo", glm::fvec2( 100, 100 ), 1.0, glm::vec3( 0.2f, 0.5f, 0.6f ) );


		glm::mat4 model = glm::mat4();
		model = glm::translate( model, glm::vec3( 2.0f, 2.0f, 0.0f ) );
		model = glm::scale( model, glm::vec3( 1.0f, 1.0f, 1.0f ) );


		std::vector<glm::vec3> localVerticesPosition;

		//get all meshes of the sphere and their vertices position
		for ( const Mesh& mesh : sphereModel.meshes ) {
			for ( const Vertex& vert : mesh.vertices ) {
				glm::vec3 localVertexPos = vert.Position;
				localVerticesPosition.push_back( localVertexPos );
			}
		}

		int sizePos = localVerticesPosition.size();

		std::vector<glm::vec4> sphereWorldPos;

		//now transform the local vertices positions in the world space via the model matrix
		for ( int i = 0; i != sizePos; ++i ) {
			glm::vec4 worldVertPos = model * glm::vec4( localVerticesPosition.at( i ), 1.0 );
			sphereWorldPos.push_back( worldVertPos );
		}

		//now resize the bounding box dependend on the vertices positions in the sphere
		renderBoundingBoxWireframe( BoundingBoxShader, sphereWorldPos );


		for ( int i = 0; i != WorldSpaceAABB.size(); i++ ) {
			if ( frustum.pointInFrustum( WorldSpaceAABB.at( i ) ) ) {
				renderSphere( SphereShader, sphereModel, model );
			}
		}

		glfwSwapBuffers( window );
		glfwPollEvents();
	}


	glDeleteVertexArrays( 1, &PLANEVAO );
	glDeleteBuffers( 1, &PLANEVBO );

	glDeleteVertexArrays( 1, &CUBEVAO );
	glDeleteBuffers( 1, &CUBEVBO );

	glDeleteVertexArrays( 1, &VAOTEXT );
	glDeleteBuffers( 1, &VBOTEXT );

	glfwTerminate();
	return 0;
}


void renderPlane( Shader &shader, unsigned int tex ) {

	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

	shader.use();

	projection = glm::perspective( glm::radians( camera.Zoom ), ( float ) SCR_WIDTH / ( float ) SCR_HEIGHT, 0.1f, 100.0f );
	view = camera.GetViewMatrix();

	shader.setMat4( "projection", projection );
	shader.setMat4( "view", view );

	//set light uniforms
	shader.setVec3( "viewPos", camera.Position );
	shader.setVec3( "lightPos", lightPos );


	// set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------
	float planeVertices[] = {
		 // positions            // normals         // texcoords
		 10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,  10.0f,  0.0f,
		-10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
		-10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,   0.0f, 10.0f,

		 10.0f, -0.5f,  10.0f,  0.0f, 1.0f, 0.0f,  10.0f,  0.0f,
		-10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,   0.0f, 10.0f,
		 10.0f, -0.5f, -10.0f,  0.0f, 1.0f, 0.0f,  10.0f, 10.0f
	};


	glGenVertexArrays( 1, &PLANEVAO );
	glGenBuffers( 1, &PLANEVBO );

	glBindVertexArray( PLANEVAO );
	glBindBuffer( GL_ARRAY_BUFFER, PLANEVBO );

	glBufferData( GL_ARRAY_BUFFER, sizeof( planeVertices ), planeVertices, GL_STATIC_DRAW );

	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof( float ) * 8, 0 );
	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof( float ) * 8, (void*) (sizeof(float) * 3 ) );
	glEnableVertexAttribArray( 2 );
	glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, sizeof( float ) * 8, ( void* ) (sizeof( float ) * 6 ) );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindVertexArray( 0 );

	glBindVertexArray( PLANEVAO );
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_2D, tex );

	glDrawArrays( GL_TRIANGLES, 0, 6 );

}



void renderSphere( Shader &shader, Model &sphere, const glm::mat4& model ){

	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

	shader.use();

	shader.setMat4( "model", model );
	shader.setMat4( "view", view );
	shader.setMat4( "projection", projection );

	sphere.draw( shader );
}


void renderBoundingBoxWireframe( Shader &shader, const std::vector<glm::vec4>& sphereWorldPos ) {

	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

	shader.use();

	float min_x, max_x;
	float min_y, max_y;
	float min_z, max_z;

	//first initial "guess"
	min_x = max_x = sphereWorldPos.at( 0 ).x;
	min_y = max_y = sphereWorldPos.at( 0 ).y;
	min_z = max_z = sphereWorldPos.at( 0 ).z;

	//now check the sphere for min and max (xyz) coordinate values
	for ( const glm::vec4 sphereVert : sphereWorldPos ) {
		if ( sphereVert.x < min_x ) { 
			min_x = sphereVert.x; 
		}
		if ( sphereVert.x > max_x ) {
			max_x = sphereVert.x;
		}
		if ( sphereVert.y < min_y ) {
			min_y = sphereVert.y;
		}
		if ( sphereVert.y > max_y ) {
			max_y = sphereVert.y;
		}
		if ( sphereVert.z < min_z ) {
			min_z = sphereVert.z;
		}
		if ( sphereVert.z > max_z ) {
			max_z = sphereVert.z;
		}
	}

	glm::vec3 size = glm::vec3( max_x - min_x, max_y - min_y, max_z - min_z );
	glm::vec3 center = glm::vec3( (min_x + max_x) / 2, (min_y + max_y) / 2, (min_z + max_z) / 2 );

	glm::mat4 model = glm::translate( glm::mat4( 1 ), center ) * glm::scale( glm::mat4(1), size );

	shader.setMat4( "model", model );
	shader.setMat4( "view", view );
	shader.setMat4( "projection", projection );

	//local space coordinate of wireframe cube
	std::vector<glm::vec3> cubeVertices;

	// back face
	cubeVertices.push_back( glm::vec3( -1.0f, -1.0f, -1.0f ) );   // bottom-left
	cubeVertices.push_back( glm::vec3( 1.0f, 1.0f, -1.0f ) );   // top-right
	cubeVertices.push_back( glm::vec3( 1.0f, -1.0f, -1.0f ) );   // bottom-right         
	cubeVertices.push_back( glm::vec3( 1.0f, 1.0f, -1.0f ) );  // top-right
	cubeVertices.push_back( glm::vec3( -1.0f, -1.0f, -1.0f ) );  // bottom-left
	cubeVertices.push_back( glm::vec3( -1.0f, 1.0f, -1.0f ) );  // top-left
	// front face
	cubeVertices.push_back( glm::vec3( -1.0f, -1.0f, 1.0f ) );  // bottom-left
	cubeVertices.push_back( glm::vec3( 1.0f, -1.0f, 1.0f ) );   // bottom-right
	cubeVertices.push_back( glm::vec3( 1.0f, 1.0f, 1.0f ) );  // top-right
	cubeVertices.push_back( glm::vec3( 1.0f, 1.0f, 1.0f ) );  // top-right
	cubeVertices.push_back( glm::vec3( -1.0f, 1.0f, 1.0f ) );   // top-left
	cubeVertices.push_back( glm::vec3( -1.0f, -1.0f, 1.0f ) );  // bottom-left
	// left face
	cubeVertices.push_back( glm::vec3( -1.0f, 1.0f, 1.0f ) );  // top-right
	cubeVertices.push_back( glm::vec3( -1.0f, 1.0f, -1.0f ) ); // top-left
	cubeVertices.push_back( glm::vec3( -1.0f, -1.0f, -1.0f ) );  // bottom-left
	cubeVertices.push_back( glm::vec3( -1.0f, -1.0f, -1.0f ) );  // bottom-left
	cubeVertices.push_back( glm::vec3( -1.0f, -1.0f, 1.0f ) );  // bottom-right
	cubeVertices.push_back( glm::vec3( -1.0f, 1.0f, 1.0f ) );  // top-right
		// right face																																	  
	cubeVertices.push_back( glm::vec3( 1.0f, 1.0f, 1.0f ) );   // top-left
	cubeVertices.push_back( glm::vec3( 1.0f, -1.0f, -1.0f ) );  // bottom-right
	cubeVertices.push_back( glm::vec3( 1.0f, 1.0f, -1.0f ) );   // top-right         
	cubeVertices.push_back( glm::vec3( 1.0f, -1.0f, -1.0f ) );  // bottom-right
	cubeVertices.push_back( glm::vec3( 1.0f, 1.0f, 1.0f ) );   // top-left
	cubeVertices.push_back( glm::vec3( 1.0f, -1.0f, 1.0f ) );  // bottom-left     
		// bottom face
	cubeVertices.push_back( glm::vec3( -1.0f, -1.0f, -1.0f ) );  // top-right
	cubeVertices.push_back( glm::vec3( 1.0f, -1.0f, -1.0f ) );   // top-left
	cubeVertices.push_back( glm::vec3( 1.0f, -1.0f, 1.0f ) );   // bottom-left
	cubeVertices.push_back( glm::vec3( 1.0f, -1.0f, 1.0f ) ); // bottom-left
	cubeVertices.push_back( glm::vec3( -1.0f, -1.0f, 1.0f ) );   // bottom-right
	cubeVertices.push_back( glm::vec3( -1.0f, -1.0f, -1.0f ) );  // top-right
		// top face
	cubeVertices.push_back( glm::vec3( -1.0f, 1.0f, -1.0f ) );  // top-left
	cubeVertices.push_back( glm::vec3( 1.0f, 1.0f, 1.0f ) );   // bottom-right
	cubeVertices.push_back( glm::vec3( 1.0f, 1.0f, -1.0f ) );// top-right     
	cubeVertices.push_back( glm::vec3( 1.0f, 1.0f, 1.0f ) );  // bottom-right
	cubeVertices.push_back( glm::vec3( -1.0f, 1.0f, -1.0f ) );  // top-left
	cubeVertices.push_back( glm::vec3( -1.0f, 1.0f, 1.0f ) );   // bottom-left   

	//the final calculated boundingbox vertices in world space
	WorldSpaceAABB = BoundingBoxInWorldSpace( cubeVertices, model );


	glGenVertexArrays( 1, &BOUNDINGVAO );
	glGenBuffers( 1, &BOUNDINGVBO );

	glBindVertexArray( BOUNDINGVAO );
	glBindBuffer( GL_ARRAY_BUFFER, BOUNDINGVBO );
	//glBufferData( GL_ARRAY_BUFFER, sizeof( cubePos ), &cubePos, GL_STATIC_DRAW );
	glBufferData( GL_ARRAY_BUFFER, cubeVertices.size() * sizeof( glm::vec3 ), &cubeVertices.front(), GL_DYNAMIC_DRAW );

	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( float ), ( void* ) 0 );

	glDrawArrays( GL_TRIANGLES, 0, 36 );

	glBindVertexArray( 0 );

}


void renderText( Shader &shader, std::string text, glm::fvec2 pos, float scale, glm::vec3 color ) {

	shader.use();

	glm::mat4 orthoProjection = glm::ortho( 0.0f, static_cast<GLfloat>(SCR_WIDTH), 0.0f, static_cast<GLfloat>(SCR_HEIGHT) );

	shader.setMat4( "projection", orthoProjection );
	shader.setVec3( "textColor", color.x, color.y, color.z );

	glActiveTexture( GL_TEXTURE0 );
	glBindVertexArray( VAOTEXT );

	// Iterate through all characters
	std::string::const_iterator c;
	
	for ( c = text.begin(); c != text.end(); c++ ) {

		Character ch = Characters[(GLchar)*c];

		GLfloat xpos = pos.x + ch.bearing.x * scale;
		GLfloat ypos = pos.y - (ch.size.y - ch.bearing.y) * scale;

		GLfloat w = ch.size.x * scale;
		GLfloat h = ch.size.y * scale;

		GLfloat vertices[6][4] = {
			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos,     ypos,       0.0, 1.0 },
			{ xpos + w, ypos,       1.0, 1.0 },

			{ xpos,     ypos + h,   0.0, 0.0 },
			{ xpos + w, ypos,       1.0, 1.0 },
			{ xpos + w, ypos + h,   1.0, 0.0 }
		};

		// Render glyph texture over quad
		glBindTexture( GL_TEXTURE_2D, ch.TextureID );
		// Update content of VBO memory
		glBindBuffer( GL_ARRAY_BUFFER, VBOTEXT );
		glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof( vertices ), vertices ); // Be sure to use glBufferSubData and not glBufferData

		glBindBuffer( GL_ARRAY_BUFFER, 0 );
		// Render quad
		glDrawArrays( GL_TRIANGLES, 0, 6 );
		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		pos.x += (ch.Advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
	}

	//unbind it afterwards
	glBindVertexArray( 0 );
	glBindTexture( GL_TEXTURE_2D, 0 );

}


std::vector<glm::vec3> BoundingBoxInWorldSpace( const std::vector<glm::vec3>& cubeVertices, glm::mat4& model ) {

	std::vector<glm::vec3> worldPosBoundingPos;

	for ( glm::vec3 vertex : cubeVertices ) {
		glm::vec4 worldPosVertex = model * glm::vec4(vertex, 1.0);
		worldPosBoundingPos.push_back( worldPosVertex );
	}

	return worldPosBoundingPos;
}


// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput( GLFWwindow *window ) {

	if ( glfwGetKey( window, GLFW_KEY_ESCAPE ) == GLFW_PRESS )
		glfwSetWindowShouldClose( window, true );

	float cameraSpeed = 2.5 * deltaTime;

	if ( glfwGetKey( window, GLFW_KEY_W ) == GLFW_PRESS )
		camera.ProcessKeyboard( FORWARD, deltaTime );
	if ( glfwGetKey( window, GLFW_KEY_S ) == GLFW_PRESS )
		camera.ProcessKeyboard( BACKWARD, deltaTime );
	if ( glfwGetKey( window, GLFW_KEY_A ) == GLFW_PRESS )
		camera.ProcessKeyboard( LEFT, deltaTime );
	if ( glfwGetKey( window, GLFW_KEY_D ) == GLFW_PRESS )
		camera.ProcessKeyboard( RIGHT, deltaTime );
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback( GLFWwindow* window, int width, int height ) {
	// make sure the viewport matches the new window dimensions; note that width and
	// height will be significantly larger than specified on retina displays.
	glViewport( 0, 0, width, height );
}


// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback( GLFWwindow* window, double xpos, double ypos ) {
	
	if ( firstMouse ) {
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement( xoffset, yoffset );
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback( GLFWwindow* window, double xoffset, double yoffset ) {
	camera.ProcessMouseScroll( yoffset );
}

// utility function for loading a 2D texture from file
// ---------------------------------------------------
unsigned int loadTexture( char const * path ) {

	unsigned int textureID;
	glGenTextures( 1, &textureID );

	int width, height, nrComponents;
	unsigned char *data = stbi_load( path, &width, &height, &nrComponents, 0 );
	if ( data ) {
		GLenum format;
		if ( nrComponents == 1 )
			format = GL_RED;
		else if ( nrComponents == 3 )
			format = GL_RGB;
		else if ( nrComponents == 4 )
			format = GL_RGBA;

		glBindTexture( GL_TEXTURE_2D, textureID );
		glTexImage2D( GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data );
		glGenerateMipmap( GL_TEXTURE_2D );

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT ); // for this tutorial: use GL_CLAMP_TO_EDGE to prevent semi-transparent borders. Due to interpolation it takes texels from next repeat
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

		stbi_image_free( data );
	} else {
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free( data );
	}

	return textureID;
}


int loadFont( void ) {

	//freetype initialization
	if ( FT_Init_FreeType( &library ) ) {
		std::cout << "FT::Error:: could not initialize freetype" << std::endl;
	}

	if ( FT_New_Face( library, "fonts/arial.ttf", 0, &face ) ) {
		std::cout << "FT::Error:: failed to load font" << std::endl;
	}

	FT_Set_Pixel_Sizes( face, 0, 48 );

	int errorcode = 0;

	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	//load only the first characters of the ASCII set
	for ( GLubyte c = 0; c != 128; c++ ) {

		if ( FT_Load_Char( face, c, FT_LOAD_RENDER ) ) {
			std::cout << "ERROR::FREETYPE: Failed to load Glyph" << std::endl;
			continue;
		}

		//generate texture for each glyph
		unsigned int texture;

		glGenTextures( 1, &texture );
		glBindTexture( GL_TEXTURE_2D, texture );

		glTexImage2D( GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer );

		// Set texture options
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );


		//store character for later use
		Character character = {
			texture,
			glm::ivec2( face->glyph->bitmap.width, face->glyph->bitmap.rows ),
			glm::ivec2( face->glyph->bitmap_left, face->glyph->bitmap_top ),
			face->glyph->advance.x
		};

		//store characters in the map afterwards for later use
		Characters.insert( std::pair<GLchar, Character>( c, character ) );
	}


	glBindTexture( GL_TEXTURE_2D, 0 );

	FT_Done_Face( face );
	FT_Done_FreeType( library );

	// Configure VAO/VBO for texture quads
	glGenVertexArrays( 1, &VAOTEXT );
	glGenBuffers( 1, &VBOTEXT );

	glBindVertexArray( VAOTEXT );
	glBindBuffer( GL_ARRAY_BUFFER, VBOTEXT );

	glBufferData( GL_ARRAY_BUFFER, sizeof( GLfloat ) * 6 * 4, NULL, GL_DYNAMIC_DRAW );
	
	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof( GLfloat ), 0 );


	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindVertexArray( 0 );

	return errorcode;
}