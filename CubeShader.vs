#version 330 core

layout ( location = 0 ) in vec3 aPos;
layout ( location = 1 ) in vec3 aNormal;
layout ( location = 2 ) in vec2 aTex;


//interface block
out VS_OUT {
	out vec3 FragPos;
	out vec3 Normal;
	out vec2 TexCoords;
} vs_out;


uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


void main(){

	vs_out.FragPos = aPos;
	vs_out.Normal = aNormal;
	vs_out.TexCoords = aTex;

	gl_Position = projection * view * model * vec4(aPos, 1.0);

}