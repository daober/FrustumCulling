
#include "Plane.h"

#include <math.h>
#include <stdio.h>


Plane::Plane( glm::vec3 &v1, glm::vec3 &v2, glm::vec3 &v3) {
	createPlane(v1,v2,v3);
}


Plane::Plane() {}

Plane::~Plane() {}


void Plane::createPlane( glm::vec3 &v1, glm::vec3 &v2, glm::vec3 &v3) {

	glm::vec3 aux1, aux2;

	aux1 = v1 - v2;
	aux2 = v3 - v2;

	normal = glm::cross( aux2, aux1 );

	normal = glm::normalize( normal );

	this->point = v2;

	d = -(glm::dot( normal, point ));
}


void Plane::setCoefficients(float a, float b, float c, float d) {

	// set the normal vector
	this->normal.x = a;
	this->normal.y = b;
	this->normal.z = c;

	//compute the lenght of the vector
	float l = normal.length();
	// normalize the vector
	this->normal = glm::normalize(normal);

	// and divide d by th length as well
	this->d = d/l;
}


float Plane::distance( glm::vec3 &p) {
	float result = (d + glm::dot( normal, p ));
	return result;
}

