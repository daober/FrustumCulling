// Plane.h
//
//////////////////////////////////////////////////////////////////////


#pragma once

#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>
#include <glm\glm\gtx\quaternion.hpp>
#include <glm/glm/gtc/type_ptr.hpp>



class Plane  {

public:

	glm::vec3 normal, point;
	float d;

	Plane::Plane( glm::vec3 &v1, glm::vec3 &v2, glm::vec3 &v3);
	Plane::Plane(void);
	Plane::~Plane();

	void createPlane( glm::vec3 &v1, glm::vec3 &v2, glm::vec3 &v3);
	void setCoefficients(float a, float b, float c, float d);
	float distance( glm::vec3 &p);

};

