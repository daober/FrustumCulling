#pragma once


#include <glm/glm/glm.hpp>
#include <glm/glm/gtc/matrix_transform.hpp>
#include <glm\glm\gtx\quaternion.hpp>
#include <glm/glm/gtc/type_ptr.hpp>

#include "Plane.h"



class Frustum {


public:

	Frustum();

	~Frustum();

	Plane planes[6];

	void setCamInternals( float angle, float ratio, float near, float far );
	void computeCamPlanes( glm::vec3 &p, glm::vec3 &l, glm::vec3 &u );

	int pointInFrustum( glm::vec3 &cubeVertices );

	static enum PLANE { TOP = 0, BOTTOM, LEFT, RIGHT, NEARP, FARP };
	enum INTERSECTION { OUTSIDE, INTERSECT, INSIDE };

private:



	glm::vec3 ntl, ntr, nbl, nbr, ftl, ftr, fbl, fbr;
	float nearD, farD, ratio, angle, tang;
	float nw, nh, fw, fh;

};

